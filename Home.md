# Team 34

- Juan Sebastián Pinzón Roncancio - js.pinzonr@uniandes.edu.co - [Link to Individual Repository](link)
- Gabriela Garcia Cardenas - g.garciac@uniandes.edu.co - [Link to Individual Repository](link)
- Isabel Carrascal - mi.carrascal@uniandes.edu.co - [Link to Individual Repository](link)
- Violett Mazurina - V.mazurina@uniandes.edu.co - [Link to Individual Repository](link)
- Daniela Ricaurte - d.ricaurte@uniandes.edu.co - [Link to Individual Repository](link)
- Martin Restrepo - m.restrepom@uniandes.edu.co - [Link to Individual Repository](link)


Problem 1: ......................


Problem 2: ......................




# Table content

0. [Contract](Contract.md)
1. [Sprint 1](Sprint1)

    - [MS1](Sprint1/MS1.md)
    - [MS2](Sprint1/MS2.md)
    - [MS3](Sprint1/MS3.md)
    - [MS4](Sprint1/MS4.md)
      
